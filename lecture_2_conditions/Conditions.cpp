﻿#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
	setlocale(LC_ALL, "Rus");

	int x;
	cin >> x;

	// Операторы сравнения
	/*
		>
		<
		>=
		<=
		==
		!=
	*/

	// true (ИСТИНА) >0, как правило, 1
	// false (ЛОЖЬ) 0

	/*if (x > 0)
	{
		cout << "Положительное" << endl;
		if (x > 50)
			cout << "X больше 50" << endl;
		else
			cout << "X меньше 50" << endl;
	}

	else if (x < 0)
	{
		cout << "Отрицательное" << endl;
	}

	else
	{
		cout << "Равно 0" << endl;
	}*/

	// Проверить, входит ли х в промежуток от 0 до 100
	/*if (x > 0)
		if (x < 100)
			cout << "Входит" << endl;
		else
			cout << "Не входит" << endl;
	else
		cout << "Не входит" << endl;*/

	// Логические операторы
	/*
		&& - И        1 && 1
		|| - ИЛИ	  0 || 1, 1 || 0, 1 || 1
		! - НЕ		  !1
	*/
	/*if (x > 0 && x < 100)
	{
		cout << "X входит в промежуток" << endl;
	}
	else
	{
		cout << "X не входит в промежуток" << endl;
	}*/

	/*if (x == 10 || x == 20)
		cout << x << endl;*/

	// Вывод месяца
	/*if (x == 1)
		cout << "Январь" << endl;
	else if (x == 2)
		cout << "Февраль" << endl;*/

	/*switch (x)
	{
		case 1: cout << "Январь" << endl;
			    break;
		case 2: cout << "Февраль" << endl;
			    break;
		case 12: cout << "Декабрь" << endl;
				 break;
		default:
			cout << "Такой цифры нет!" << endl;
	}*/

	// Тернарный оператор
	// int y = (x > 0) ? 10 : -10;
	// cout << y << endl;

	// Проверка на четность
	/* if (x % 2 == 0)
	{
		cout << "Четное" << endl;
	}*/

	return 0;
}
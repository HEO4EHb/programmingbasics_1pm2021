#include <pig.hpp>

namespace mt
{
	Pig::Pig(int x, int y, float r)
	{
		m_x = x;
		m_y = y;
		m_r = r;
	}

	bool Pig::Setup()
	{
		if (!m_texture.loadFromFile("img/peppa.png"))
		{
			std::cout << "ERROR when loading peppa.png" << std::endl;
			return false;
		}

		m_pig = new sf::Sprite();
		m_pig->setTexture(m_texture);
		m_pig->setOrigin(m_r, m_r);
		m_pig->setPosition(m_x, m_y);
		m_pig->setScale(0.2, 0.2);

		return true;
	}

	Pig::~Pig()
	{
		if (m_pig != nullptr)
			delete m_pig;
	}

	sf::Sprite* Pig::Get() { return m_pig; }

	int Pig::GetX() { return m_x; }
	int Pig::GetY() { return m_y; }
	float Pig::GetR() { return m_r; }
}
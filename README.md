# Основы программирования 1ПМ осень 2021

## Лекции (осень)  
Лекция 1  
https://www.youtube.com/watch?v=6z-D8akDzD4

Лекция 2  
https://www.youtube.com/watch?v=U9On5Dn8JTE

Лекция 3 Циклы   
см в тимсе https://teams.microsoft.com/l/team/19%3abeN2iz4TmrcJYwGj0DL3RsSEGDCjBVctgg7mtX22a6A1%40thread.tacv2/conversations?groupId=26d049b9-e0bd-4e6e-a8fb-c49dc4f1bb6f&tenantId=6bf462b9-ace0-435b-94e5-2bb4d5189db7  
https://youtu.be/F2WJIv5wdGs

Лекция 4 Массивы  
https://youtu.be/Um7b2EDmfpc

Лекция 5 Строки  
https://youtu.be/qMU4gjkhdC4 https://youtu.be/C4bJmR6OovU   

Лекция 6 Функции  
https://www.youtube.com/watch?v=hm5otpCTX6g  

Лекция 7 Математика Гит Cmake  
https://youtu.be/5GWHyj0APus  

Лекция 8 Ссылки, указатели, динамическая память  
https://youtu.be/H461o3UMF2s  

Лекция 9 Основы работы с графикой на примере библиотеки SFML  
https://youtu.be/A_FpftIxSWE   

Лекция 10 Основы ООП  
https://youtu.be/T65Vzq220fY   

Лекция 11 Улучшение кода при помощи ООП  
https://youtu.be/nrcasUXmNsM  

Лекция 12 Геймплей  
https://youtu.be/LdB25WJcZXE   

Лекция 13 Релиз  
https://youtu.be/hHAnUya6yQE  

## Лекции (весна)   
Лекция 14 Введение в алгоритмы и структуры данных  
https://youtu.be/Xo7IwShVnHk  

Лекция 15 Рекурсия  
https://www.youtube.com/watch?v=r6L4tByUSKI 

Лекция 16 Быстрые алгоритмы сортировки  
https://youtu.be/5LEbbhadYRE    

Лекция 17 Бинарный поиск и бисекция  
https://youtu.be/TKowebtmxWM  

Лекция 18 Повторение указателей и ссылок  
https://youtu.be/vq-wfjfu7p8  

Лекция 19 Односвязный список  
https://youtu.be/VrZ0_f4VLfg  

Лекция 20 Бинарное дерево поиска  
https://youtu.be/VWjbnKFVczo  

## Полезные ссылки  

Дополнительные материалы по С++ (просто и понятно)  
https://ravesli.com/uroki-cpp/#toc-0

Дополнительные материалы для тех, кто хочет чуть-чуть вперед  
https://www.youtube.com/watch?v=2PM4TgCZIQs&list=PLys0IdlMg6XcwfGUf9Z5QUjKH9iM1T8PG

Скачать Visual Studio 2019 Community  
https://visualstudio.microsoft.com/ru/vs/

Скачать CMake  
https://cmake.org/download/  

Пример подключения библиотеки SFML для VS 2019 (Сделайте fork репозитория или скачайте и запушьте его в свой репозиторий)  
https://gitlab.com/mishklgpmi/sfml_sample  

Документация SFML  
https://www.sfml-dev.org/tutorials/2.5/ 

Установка SFML в вижак ручками  
https://www.sfml-dev.org/tutorials/2.5/start-vc.php  


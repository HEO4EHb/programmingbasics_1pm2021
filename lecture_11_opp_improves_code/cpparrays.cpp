﻿#include <iostream>
#include <array>
#include <vector>

int main()
{
	// c-array
/*	int n;
	int mas[10];

	// std::array
	std::array<int, 10> arr;	// Шаблон
	for (int i = 0; i < 10; i++)
		std::cin >> arr[i];

	std::cout << arr[2] << std::endl;

	std::cout << arr.size() << std::endl;

	std::array<double, 10> arr1;

	// динамический массив
	int* dyn_arr = new int[10];

	// 

	delete[] dyn_arr;*/

	// std::vector
	std::vector<int> vec;
	vec.push_back(1);		// Добавление в конец
	vec.push_back(2);
	vec.push_back(3);

	//for (int i = 0; i < vec.size(); i++)	// size() - размер вектора
	//	std::cout << vec[i] << std::endl;

	// auto - автоматическое определение типа элемента
	// auto& - ссылка
	// foreach
	for (const auto& v : vec)		// если элементы менять не нужно, то исп. const auto&
	{
		std::cout << v << std::endl;
	}

	std::cout << "Delete" << std::endl;
	// Удаление
	vec.erase(vec.begin() + 1);		// vec.begin() - начало вектора (итератор)

	for (int i = 0; i < vec.size(); i++)	// size() - размер вектора
		std::cout << vec[i] << std::endl;

	std::cout << "Insert" << std::endl;
	// Дублирование
	vec.insert(vec.begin() + 1, 1000);	// 
	for (int i = 0; i < vec.size(); i++)	// size() - размер вектора
		std::cout << vec[i] << std::endl;

	std::cout << "Clear" << std::endl;

	// Очистка
	vec.clear();
	for (int i = 0; i < vec.size(); i++)	// size() - размер вектора
		std::cout << vec[i] << std::endl;

	return 0;
}

﻿#include <iostream>
#include <fstream>

#define N 1000   // определить константу N равной 100
#define M 100

int main()
{
    std::ifstream in("input.txt"); // r, rb
    std::ofstream out("output.txt"); // флаги открытия файлов w, a, wb

    /*int n;
    in >> n;    // указатель на открытый файл, std::cin
    for (int i = 0; i < n; i++)
    {
        int x;
        in >> x;
        int y = x * x;
        std::cout << y << std::endl;
        out << y << " ";
    }*/

    // Массивы
    // Фиксированный массив, память выделяется на этапе компиляцию
    //const int N_max = 100; // const значит, что переменная никогда не поменяется
    /*int n;
    in >> n;
    int mas[N];

    for (int i = 0; i < n; i++)
        in >> mas[i];

    // Обработка массива
    // Сортировка обменами
    for(int i=0;i<n-1;i++)
        for(int j=i+1;j<n;j++)
            if (mas[i] < mas[j])        // > по возрастанию, < по убыванию, <=, >=
            {
                int tmp = mas[i];       // то же самое std::swap(mas[i], mas[j]);
                mas[i] = mas[j];
                mas[j] = tmp;
            }

    for (int i = 0; i < n; i++)   // Range check error (выход за границы массива)
        out << mas[i] << " ";*/

    /*// Двумерный массив
    int matrix[N][M]; // 4 * 1000 * 100 = 400 кб
    int n, m;
    in >> n >> m;

    // Считывание матрицы
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            in >> matrix[i][j];

    // Обработка матрицы - поиск максимального элемента матрицы
    int max = matrix[0][0];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            if (max < matrix[i][j])
                max = matrix[i][j];

    // Вывод
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
            out << matrix[i][j] << " ";

        out << std::endl;
    }
    out << "Max elem = " << max;*/
	
	    // Дан массив, 1000, по последней цифре, по сумме цифр, по самим числам


    // Ввод
    int mas[N], last[N], sum[N];
    int n;
    std::cin >> n;

    for (int i = 0; i < n; i++)
    {
        std::cin >> mas[i];
        
        last[i] = mas[i] % 10;

        sum[i] = 0;
        int x = mas[i];
        while (x > 0)
        {
            sum[i] += x % 10;
            x /= 10;
        }
    }

    // Обработка
    // Сортировка по сложному ключу
    for(int i=0;i<n-1;i++)
        for (int j = i + 1; j < n; j++)
        {
            /*if (last[i] > last[j])
            {
                std::swap(mas[i], mas[j]);
                std::swap(last[i], last[j]);
                std::swap(sum[i], sum[j]);
            }
            else if (last[i] == last[j] && sum[i] > sum[j])
            {
                std::swap(mas[i], mas[j]);
                std::swap(last[i], last[j]);
                std::swap(sum[i], sum[j]);
            }
            else if (last[i] == last[j] && sum[i] == sum[j] && mas[i] > mas[j])
            {
                std::swap(mas[i], mas[j]);
                std::swap(last[i], last[j]);
                std::swap(sum[i], sum[j]);
            }*/

            if (last[i] > last[j] ||
                (last[i] == last[j] && sum[i] > sum[j]) ||
                (last[i] == last[j] && sum[i] == sum[j] && mas[i] > mas[j]))
            {
                std::swap(mas[i], mas[j]);
                std::swap(last[i], last[j]);
                std::swap(sum[i], sum[j]);
            }
        }
}